const createProductTable = `
    CREATE TABLE IF NOT EXISTS "products" (
        "id" SERIAL,
        "name" VARCHAR(100) NOT NULL,
        "price" INTEGER NOT NULL,
        PRIMARY KEY (id)
    );`;
//console.log(createProductTable);
export default { createProductTable };