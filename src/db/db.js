import pg from "pg";
import queries from "./queries.js";
import dotenv from "dotenv/config";
const isProd = process.env.NODE_ENV === "production";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

const fakeProductID = Math.floor(Math.random() * 10000);

export const pool = new pg.Pool({
    host: PG_HOST,
    port: 5432,
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        error.name = "dbError";
        throw error;
    } finally {
        client.release();
    }
};

export const createProductTable = async () => {
    await executeQuery(queries.createProductTable);
    //console.log("Product table initialized succesfully.");
};

export const insertIntoProducts = async () => {
    //await executeQuery(`INSERT INTO products (id, name, price) VALUES (2, 'Hilavitkutin', 100000);`);
    await executeQuery(`INSERT INTO products (id, name, price) VALUES (${fakeProductID}, 'Hilavitkutin', 100000);`);
    //console.log("inserted product successfully");
};

export const selectFromProducts = async () => {
    await executeQuery(`SELECT * FROM products WHERE id=${fakeProductID};`);
    //console.log("selected products successfully");
};

export const updateProducts = async () => {
    await executeQuery(`UPDATE products SET name='Vitkahilutin' WHERE id=${fakeProductID};`);
    //console.log("updated product successfully");
};

export const selectAllProducts = async () => {
    await executeQuery("SELECT * FROM products;");
    //console.log("selected products successfully");
};

export const deleteProduct = async () => {
    await executeQuery(`DELETE FROM products WHERE id=${fakeProductID};`);
    //console.log("deleted products successfully");
};

export default executeQuery;