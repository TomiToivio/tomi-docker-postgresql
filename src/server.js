// Testing
import express from "express";
import { createProductTable, insertIntoProducts, selectFromProducts, updateProducts, selectAllProducts, deleteProduct } from "./db/db.js";

createProductTable();

const server = express();
insertIntoProducts();
selectFromProducts();
updateProducts();
selectAllProducts();
deleteProduct();

//console.log(process.env);
const PORT = process.env.PORT || 3000;

server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});

export default server;