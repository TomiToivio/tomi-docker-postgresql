import { jest } from "@jest/globals";
import request from "supertest";
import server from "../src/server.js";

describe("Testing Server", () => {
    it("Root responds OK", async () => {
        const response = await request(server).get("/");
        expect(response.status).toBe(200);
        //expect(response.status).toBe(404);
    });
});
